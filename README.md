# Getting Started (Ionic Website)


Install Ionic and Cordova:

    npm install -g cordova@latest ionic@latest


Create a new Ionic app:

    ionic start my-first-app tabs

The last argument is a tamplate name. Valid starter templates are blank, tabs, sidemenu, super, conference, tutorial, aws, etc. 

_Note: Remove .git from the generated folder._


Run the app in the local env:

    ionic serve


Add the "browser" platform:

    ionic cordova platform add browser


Create a production build for deployment.

    ionic cordova build browser --prod --release

This creates deployable content in the `platforms/browser/www` folder.





_tbd_


## References

* [Get started with Ionic Framework](https://ionicframework.com/getting-started/)
* [Ionic CLI](https://ionicframework.com/docs/cli/)




